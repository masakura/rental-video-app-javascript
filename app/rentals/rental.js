export class Rental {
  /**
     * @param {Movie} movie
     * @param {int} daysRented
     */
  constructor(movie, daysRented) {
    this.movie = movie;
    this.daysRented = daysRented;
  }
}
