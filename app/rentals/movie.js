export class Movie {
  /**
   * @param {string} title
   * @param {int} priceCode
   */
  constructor(title, priceCode) {
    this.title = title;
    this.priceCode = priceCode;
  }
}
