export class Customer {
  /**
   * @param {string} name
   */
  constructor(name) {
    this.name = name;
  }
}
